﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Changes the rotation of the light depending on player position (z axis)
public class SunRotation : MonoBehaviour {
	private Transform playerTransform;

	// Use this for initialization
	void Start () {
		Vector3 playerPos = GameObject.Find("FPSController").transform.position;
		playerTransform = GameObject.Find("FPSController").transform;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 playerPos = playerTransform.position;
		float playerZ = playerPos.z;
		float degree = (float)1.5*playerZ;
		
		Quaternion currentPos = transform.rotation;
		Quaternion targetpos = Quaternion.Euler(new Vector3(degree, 0, 0));
		transform.rotation = Quaternion.Slerp(currentPos,targetpos,0.1F);
	}
	
	//We want to rotate the lighting about 160 degree for everz 100m along the z axis
	//has to add degrees when going further along the z axis

}
